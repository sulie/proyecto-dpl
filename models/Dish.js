let mongoose = require('mongoose');
let Schema = mongoose.Schema;

// Esquema
let dishSchema = new Schema({
    name: String,
    image: String,
    category: String,
    label: String,
    price: String,
    featured: Boolean,
    description: String,
    comments: [
        {
            rating: Number,
            comment: String,
            author: String,
            date: Date
        }
    ]
});

// Muestra todos los platos
dishSchema.statics.allDishes = function (cb) {
    return this.find({}, cb);
}

// Inserta en la base de datos
dishSchema.statics.add = function (aDish, cb) {
    return this.create(aDish, cb);
}

// Encuentra por _id
dishSchema.statics.findById = function (dishId, cb) {
    return this.findOne({_id: dishId}, cb); // Encuentra el primero que coincida con los filtros de la búsqueda
}

// Borra por _id
dishSchema.statics.removeById = function (dishId, cb) {
    return this.deleteOne({_id: dishId}, cb); // Borra el primero que coincida con los filtros de la búsqueda
}

module.exports = mongoose.model("Dish", dishSchema);