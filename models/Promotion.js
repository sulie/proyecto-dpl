let mongoose = require('mongoose');
let Schema = mongoose.Schema;

// Esquema
let promotionSchema = new Schema({
    name: String,
    image: String,
    label: String,
    price: Number,
    featured: Boolean,
    description: String,
});

// Encuentra todas las promociones
promotionSchema.statics.allPromotions = function (cb) {
    return this.find({}, cb);
}

// Inserta en la base de datos
promotionSchema.statics.add = function (aPromotion, cb) {
    return this.create(aPromotion, cb);
}

// Encuentra por _id
promotionSchema.statics.findById = function (promotionId, cb) {
    return this.findOne({_id: promotionId}, cb); // Encuentra el primero que coincida con los filtros de la búsqueda
}

// Borra por _id
promotionSchema.statics.removeById = function (promotionId, cb) {
    return this.deleteOne({_id: promotionId}, cb); // Borra el primero que coincida con los filtros de la búsqueda
}

module.exports = mongoose.model("Promotion", promotionSchema);