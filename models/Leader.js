let mongoose = require('mongoose');
let Schema = mongoose.Schema;

// Esquema
let leaderSchema = new Schema({
    name: String,
    image: String,
    designation: String,
    abbr: String,
    featured: Boolean,
    description: String,
});

// Encuentra todos los leaders
leaderSchema.statics.allLeaders = function (cb) {
    return this.find({}, cb);
}

// Inserta en la base de datos
leaderSchema.statics.add = function (aLeader, cb) {
    return this.create(aLeader, cb);
}

// Encuentra por _id
leaderSchema.statics.findById = function (leaderId, cb) {
    return this.findOne({_id: leaderId}, cb); // Encuentra el primero que coincida con los filtros de la búsqueda
}
// Borra por _id
leaderSchema.statics.removeById = function (leaderId, cb) {
    return this.deleteOne({_id: leaderId}, cb); // Borra el primero que coincida con los filtros de la búsqueda
}

module.exports = mongoose.model("Leader", leaderSchema);