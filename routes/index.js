var express = require('express');
var router = express.Router();

// Página de inicio - Solo para el CRUD visual en web
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Express' });
});

module.exports = router;
