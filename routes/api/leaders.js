let express = require('express');
let router = express.Router();
let leaderControllerAPI = require("../../controller/api/leaderControllerAPI");

router.get("/", leaderControllerAPI.leader_list); // Muestra todos los elementos
router.get("/find", leaderControllerAPI.leader_find); // Muestra el elemento seleccionado por _id
router.post("/create", leaderControllerAPI.leader_create); // Crea un elemento
router.delete("/delete", leaderControllerAPI.leader_delete); // Borra un elemento
router.put("/update", leaderControllerAPI.leader_update); // Actualiza un elemento
module.exports = router;