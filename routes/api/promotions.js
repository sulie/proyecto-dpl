let express = require('express');
let router = express.Router();
let promotionControllerAPI = require("../../controller/api/promotionControllerAPI");

router.get("/", promotionControllerAPI.promotion_list); // Muestra todos los elementos
router.get("/find", promotionControllerAPI.promotion_find); // Muestra el elemento seleccionado por _id
router.post("/create", promotionControllerAPI.promotion_create); // Crea un elemento
router.delete("/delete", promotionControllerAPI.promotion_delete); // Borra un elemento
router.put("/update", promotionControllerAPI.promotion_update); // Actualiza un elemento
module.exports = router;