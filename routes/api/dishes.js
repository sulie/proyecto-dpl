let express = require('express');
let router = express.Router();
let dishControllerAPI = require("../../controller/api/dishControllerAPI");

router.get("/", dishControllerAPI.dish_list); // Muestra todos los elementos
router.get("/find", dishControllerAPI.dish_find); // Muestra el elemento seleccionado por _id
router.post("/create", dishControllerAPI.dish_create); // Crea un elemento
router.post("/create_comment", dishControllerAPI.dish_comment); // Crea un comentario para un plato
router.delete("/delete", dishControllerAPI.dish_delete); // Borra un elemento
router.delete("/delete_comment", dishControllerAPI.delete_comment); // Borra un comentario para un plato
router.put("/update", dishControllerAPI.dish_update); // Actualiza un elemento
router.put("/update_comment", dishControllerAPI.update_comment); // Actualiza un comentario para un plato
module.exports = router;