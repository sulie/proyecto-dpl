let Leader = require("../../models/Leader");

// Muestra todos los leaders
exports.leader_list = function (req,res) {
    Leader.allLeaders(function (err, leaders) {
        if (err) res.send(500, err.message);
        res.status(200).json({
            leaders: leaders
        });
    });
};

// Encuentra a un leader por _id
exports.leader_find = function (req,res) {
    Leader.findById(req.body._id, function (err,leader) {
        if (err) res.send(500, err.message);
        res.status(200).json({
            leader: leader
        })
    });
}

// Crea un leader con los campos que vienen por el body de la petición
exports.leader_create = function(req,res) {
    // Objeto a insertar
    let leader = new Leader({
        name: req.body.name,
        image: req.body.image,
        designation: req.body.designation,
        abbr: req.body.abbr,
        featured: req.body.featured,
        description: req.body.description
    });

    Leader.add(leader, function (err, leader) { // Inserción
        if (err) res.send(500, err.message);
        res.status(201).json({ // Codigo 201 == CREATE con éxito
            leader: leader
        })
    });
};

// Borra un leader por _id
exports.leader_delete = function(req,res){
    let leader = req.body._id; // Target

    Leader.removeById(leader, function (err, leader) { // Borrado del target
        if (err) res.send(500, err.message);
        res.status(204).send();
    });
};

// Actualiza el leader por su _id
exports.leader_update = function(req,res){
    Leader.findById(req.body._id, function (err, leader) { // Target
        if (err) res.send(500, err.message);
        
        // Nuevo registro modificado
        let modified = {
            name: req.body.name,
            image: req.body.image,
            designation: req.body.designation,
            abbr: req.body.abbr,
            featured: req.body.featured,
            description: req.body.description
        }
        Leader.update({_id: leader._id}, modified, function (error, doc) { // Actualización
            if (error) res.send(500, err.message);
            res.status(200).send();
        });
    });
};