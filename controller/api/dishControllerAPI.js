let Dish = require("../../models/Dish");

// Devuelve todos los platos
exports.dish_list = function (req,res) {
    Dish.allDishes(function (err, dishes) {
        if (err) res.send(500, err.message);
        res.status(200).json({
            dishes: dishes
        })
    });
};

// Encuentra un plato por _id en el body
exports.dish_find = function (req,res) {
    Dish.findById(req.body._id, function (err,dish) {
        if (err) res.send(500, err.message);
        res.status(200).json({
            dish: dish
        })
    });
}

// Crea un plato mediante un JSON en el body con los siguientes parámetros basados en el modelo
exports.dish_create = function(req,res) {

    // Objeto que será insertado en el documento
    let dish = new Dish({
        name: req.body.name,
        image: req.body.image,
        category: req.body.category,
        label: req.body.label,
        price: req.body.price,
        featured: req.body.featured,
        description: req.body.description,
    });

    // Inserción
    Dish.add(dish, function (err, dish) {
        if (err) res.send(500, err.message);
        res.status(201).json({ // Codigo 201 == CREATE con éxito
            dish: dish
        })
    });
};

// Añade comentarios a un plato actualizando su campo "comments"
exports.dish_comment = function(req,res) {
    Dish.findByIdAndUpdate(req.body._id, {
        $push: { // Método para añadir platos al array "comments"
            comments: [
                {
                    rating: req.body.rating,
                    comment: req.body.comment,
                    author: req.body.author,
                    date: req.body.date
                }
            ]
        }
    }, function (err,dishes) {
        if (err) {res.status(500).send(err.message)}
        res.status(204).send();
    });
}

// Borra un comentario de un plato actualizandolo
exports.delete_comment = function(req,res) {
    Dish.findByIdAndUpdate(req.body._id, {
        $pull: { // Método para sacar del array el elemento deseado
            comments: { _id: req.body.comment_id}
        }
    }, function (err,dishes) {
        if (err) {res.status(500).send(err.message)}
        res.status(204).send();
    });
}

// Actualiza un comentario actualizando al plato
exports.update_comment = function(req,res) {
    Dish.update(
        {'comments._id': req.body.comment_id}, // Target (_id del comentario, independiente del plato asociado ya que es único)
        
        {'$set': { // Método para cambiar el valor de los campos
            'comments.$.rating': req.body.rating,
            'comments.$.comment': req.body.comment,
            'comments.$.author': req.body.author,
            'comments.$.date': req.body.date
        }
    }, 
    function (err){
        if (err) {res.status(500).send(err.message)}
        res.status(200).send();
    })
}

// Borra un plato mediante su _id y el método del modelo Dish "removeById"
exports.dish_delete = function(req,res){
    let dish = req.body._id; // Obtenemos el target

    Dish.removeById(dish, function (err, dish) {
        if (err) res.send(500, err.message);
        res.status(204).send();
    });
};

// Actualiza el plato mediante su _id
exports.dish_update = function(req,res){
    Dish.findById(req.body._id, function (err, dish) { // Encuentra al target
        if (err) res.send(500, err.message);
        
        // Nuevo registro modificado
        let modified = {
            name: req.body.name,
            image: req.body.image,
            category: req.body.category,
            label: req.body.label,
            price: req.body.price,
            featured: req.body.featured,
            description: req.body.description,
            comments: [
                req.body.rating, 
                req.body.comment,
                req.body.author,
                req.body.date
            ]
        }
        Dish.update({_id: dish._id}, modified, function (error, doc) { // Actualización en la base de datos
            if (error) res.send(500, err.message);
            res.status(200).send();
        });
    });
};