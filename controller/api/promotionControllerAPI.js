let Promotion = require("../../models/Promotion");

// Muestra todas las promociones
exports.promotion_list = function (req,res) {
    Promotion.allPromotions(function (err, promotions) {
        if (err) res.send(500, err.message);
        res.status(200).json({
            promotions: promotions
        });
    });
};

// Encuentra una promoción por _id
exports.promotion_find = function (req,res) {
    Promotion.findById(req.body._id, function (err,prom) {
        if (err) res.send(500, err.message);
        res.status(200).json({
            promotion: prom
        })
    });
}

// Crea una promoción mediante los campos que vengan por el body de la petición
exports.promotion_create = function(req,res) {
    // Objeto a insertar
    let promotion = new Promotion({
        name: req.body.name,
        image: req.body.image,
        label: req.body.label,
        price: req.body.price,
        featured: req.body.featured,
        description: req.body.description
    });

    Promotion.add(promotion, function (err, promotion) { // Inserción
        if (err) res.send(500, err.message);
        res.status(201).json({ // Codigo 201 == CREATE con éxito
            promotion: promotion
        })
    });
};

// Elimina una promoción mediante su _id
exports.promotion_delete = function(req,res){
    let promotion = req.body._id; // Target

    Promotion.removeById(promotion, function (err, promotion) { // Borrado del target
        if (err) res.send(500, err.message);
        res.status(204).send();
    });
};

//Actualiza una promoción mediante su _id
exports.promotion_update = function(req,res){
    Promotion.findById(req.body._id, function (err, promotion) { // Target
        if (err) res.send(500, err.message);
        
        // Nuevo registro modificado
        let modified = {
            name: req.body.name,
            image: req.body.image,
            label: req.body.label,
            price: req.body.price,
            featured: req.body.featured,
            description: req.body.description
        }
        Promotion.update({_id: promotion._id}, modified, function (error, doc) { // Actualización
            if (error) res.send(500, err.message);
            res.status(200).send();
        });
    });
};