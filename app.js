// Instancia de los módulos de node
var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
var mongoose = require('mongoose');

// Enrutadores -> Controladores
var indexRouter = require('./routes/index');
var usersRouter = require('./routes/users');
var dishAPIRouter = require('./routes/api/dishes');
var promotionAPIRouter = require('./routes/api/promotions');
var leaderAPIRouter = require('./routes/api/leaders');

var app = express();

// Configuración de las vistas - Solo para CRUD visual en web
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');

// Middleware y configuraciones
app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

// Rutas -> Peticiones
app.use('/', indexRouter);
app.use('/users', usersRouter);
app.use('/api/dishes', dishAPIRouter);
app.use('/api/promotions', promotionAPIRouter);
app.use('/api/leaders', leaderAPIRouter);

//Conexión y configuración MongoDB -> Mongoose
mongoose.connect('mongodb://localhost/proyecto-dpl', {useUnifiedTopology: true, useNewUrlParser: true});
mongoose.Promise = global.Promise;

var db = mongoose.connection;

db.on("error", console.error.bind('Error de conexión con MongoDB'));

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;
